'use strict';
define([
        'angular',
        'angularRoute'],

    function(angular) {

        var service = angular.module('sessionService', ['ngRoute']).service('tvService', function(){

            var tvService = {};
            var tvdata = new tvData();

            tvService.getData = function() { return tvdata; }
            tvService.setData = function(obj) { tvdata = obj; }

            return tvService;
        });

        return service;
    });
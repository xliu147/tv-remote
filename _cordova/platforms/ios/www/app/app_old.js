
var tvremoeteApp = angular.module('cordovaApp', ['ngRoute']);


tvremoeteApp.config(['$routeProvider', function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'views/ipinput.html',
            controller: 'mainController'
        })
        .when('/pin', {
            templateUrl: 'views/pin.html',
            controller: 'handshakeController'
        })


}]);

tvremoeteApp.run(function($rootScope, $templateCache) {
    $rootScope.$on('$viewContentLoaded', function() {
        $templateCache.removeAll();
    });
});

//DATA MODEL
var tvData = function (obj) {
    var self = this;

    if (!obj) {
        self.ipAddress = null;
        self.pin = null;
        self.friendlyname = null;
        self.appID = '721b6fce-4ee6-48ba-8045-955a539edadb';
        self.deviceinfo = null;

    } else {
        self = obj;
    }

    return self;
}

//DATA SERVICE
tvremoeteApp.service('tvService', function(){

    var tvService = {};
    var tvdata = new tvData();

    tvService.getData = function() { return tvdata; }
    tvService.setData = function(obj) { tvdata = obj; }

    return tvService;
});




tvremoeteApp.controller('mainController', ['$scope','$location','tvService','$http', function($scope,$location,tvService,$http) {
   $scope.message = "enter IP address";

    //screen 1
    $scope.discover = function() {

        $scope.model = tvService.getData();

        $scope.model.ipAddress = $scope.ipAddress;


        var url = 'http://' + $scope.model.ipAddress + ':8001/ms/1.0/';
        $http.get(url)
            .then(function (response) {
                $scope.model.deviceinfo = response.data;
                tvService.setData($scope.model);
                $location.path('/pin');
            });


    }




}]);

buildPairingStepUri = function(step, data) {
    var path = '/ws/pairing?step=' + step + '&app_id=' + data.appID + '&device_id=' + data.deviceinfo.DeviceID;
    return 'http://' + data.ipAddress + ':8080' + path;
}

tvremoeteApp.controller('handshakeController', ['$scope','$location','tvService','$http', function($scope,$location,tvService,$http) {
    //Retrieve data model
    $scope.model = tvService.getData();







    $scope.deviceName = $scope.model.deviceinfo.ModelDescription;
    //Step 0
    console.log(buildPairingStepUri(0,$scope.model));
    $http.get(buildPairingStepUri(0,$scope.model))
        .then(function (response) {
            console.log(response);
        });




    $scope.handshake = function() {
        $scope.model = tvService.getData();
        $scope.model.pin = $scope.pin;
        tvService.setData($scope.model);

        console.log($scope.model);
    }
}]);
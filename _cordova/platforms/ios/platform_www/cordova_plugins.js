cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "com.scott.serviceDiscovery.serviceDiscovery",
    "file": "plugins/com.scott.serviceDiscovery/www/serviceDiscovery.js",
    "pluginId": "com.scott.serviceDiscovery",
    "clobbers": [
      "serviceDiscovery"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "com.scott.serviceDiscovery": "0.2.0"
};
// BOTTOM OF METADATA
});
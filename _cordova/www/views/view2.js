'use strict';

define([
    'angular',
    'angularRoute'
], function(angular) {


    angular.module('tvremoeteApp.view2', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {

        $routeProvider.when('/pin', {
                templateUrl: 'views/pin.html',
                controller: 'handshakeController'
            });
        }])
        // We can load the controller only when needed from an external file
    .controller('handshakeController', ['$scope','$location','tvService','$http', function($scope,$location,tvService,$http) {
        //Retrieve data model

        try {
            $scope.model = tvService.getData();

            if ($scope.model.deviceinfo == null) {
                $location.path('/');
            }

            $scope.deviceName = $scope.model.deviceinfo.ModelDescription;

            //DISPLAY PIN ON TV
            $http.post('http://' + $scope.model.ipAddress + ':8080/ws/apps/CloudPINPage');

            $scope.handshake = function () {
                $scope.model = tvService.getData();
                $scope.model.pin = $scope.pin;
                tvService.setData($scope.model);

                (function () {

                    //STEP 0
                    $http.get(buildPairingStepUri(0, $scope.model) + '&type=1')
                        .then(function (response) {
                            console.log('Step 0, responseStatus', response.status)
                        });


                    //STEP 1
                    var serverHello = generateServerHello($scope.model.userID, $scope.model.pin)
                    var uri = buildPairingStepUri(1, $scope.model);
                    var data = JSON.stringify({
                        'auth_Data': {
                            'auth_type': 'SPC',
                            'GeneratorServerHello': serverHello
                        }
                    });

                    $http.post(uri, data).then(function (response) {
                        console.log('step 1');
                        var authData = JSON.parse(response.data.auth_data);
                        console.log(authData);

                        if (parseClientHello(authData.GeneratorClientHello) !== 0) {
                            console.error('Invalid PIN Entered')
                            throw Error('Invalid PIN entered')
                        }

                        var identity = {
                            sessionId: authData.request_id,
                            aesKey: getKey()
                        }
                        $scope.model.identity = identity;

                        console.log('step 2');
                        var serverAck = generateServerAcknowledge();
                        var uri = buildPairingStepUri(2, $scope.model);
                        var data = JSON.stringify({
                            'auth_Data': {
                                'auth_type': 'SPC',
                                'request_id': $scope.model.identity.sessionId,
                                'ServerAckMsg': serverAck
                            }
                        });

                        $http.post(uri, data).then(function (response) {
                            console.log('server ack');
                            console.log(response);
                            var authData = JSON.parse(response.data.auth_data);

                            var clientAck = parseClientAcknowledge(authData.ClientAckMsg);
                            if (!clientAck) {
                                throw Error('failed to acknowledge client')
                            }
                            $scope.model.identity.sessionId = authData.session_id;

                            //HIDE PIN PAGE
                            $http.delete('http://' + $scope.model.ipAddress + ':8080/ws/apps/CloudPINPage/run')
                                .then(function (response) {
                                    console.log('Pin page closed', response);
                                    $scope.model.paired = true;
                                });

                            //OPEN WEB SOCKET
                            var socket = openSocket();

                        })
                    })
                })();
            }

            var openSocket = function () {
                $http.post("http://" + $scope.model.ipAddress + ":8000/socket.io/1")
                    .then(function (response) {
                        const handshake = response.data.split(':');

                        //update state
                        $scope.model.connectionstate = true;
                        var socket = new WebSocket("ws://" + $scope.model.ipAddress + ":8000/socket.io/1/websocket/" + handshake[0].toString());

                        if (!socket) {
                            console.log('unable to open web socket');
                        }

                        socket.addEventListener('onclose', function (event) {
                            alert('socket closed');
                            $scope.model.connectionstate = false;
                        });

                        socket.addEventListener('open', function (event) {
                            var identity = $scope.model.identity;
                            socket.onmessage = function (event) {
                                if (event.data == "2::") {
                                    console.log('2:: KEEP ALIVE');
                                }
                                if (event.data == "1::") {

                                    socket.send('1::/com.samsung.companion');
                                    socket.send(buildEmitMessage(
                                        'registerPush',
                                        encryptData(identity.aesKey, identity.sessionId, {
                                            eventType: 'EMP',
                                            plugin: 'SecondTV'
                                        })
                                    ));

                                    socket.send(buildEmitMessage(
                                        'registerPush',
                                        encryptData(identity.aesKey, identity.sessionId, {
                                            eventType: 'EMP',
                                            plugin: 'RemoteControl'
                                        })
                                    ));

                                    socket.send(buildEmitMessage(
                                        'callCommon',
                                        encryptData(identity.aesKey, identity.sessionId, {
                                            method: 'POST',
                                            body: {
                                                plugin: 'NNavi',
                                                api: 'GetDUID',
                                                version: '1.000'
                                            }
                                        })
                                    ));
                                }
                            }

                            $scope.sendKeyAction = function (key) {
                                console.log('sending key');
                                sendKey(socket, key);
                                if (socket.readyState == 3) {
                                    openSocket();
                                }


                            }
                        });
                    });
            }

            var sendKey = function (socket, key) {
                console.log('socket state');

                if (socket.readyState == 1 && socket != null && $scope.model.deviceinfo.DUID != null) {
                    console.log('sending message');
                    socket.send(buildEmitMessage(
                        'callCommon',
                        encryptData($scope.model.identity.aesKey, $scope.model.identity.sessionId, {
                            method: 'POST',
                            body: {
                                plugin: 'RemoteControl',
                                version: '1.000',
                                api: 'SendRemoteKey',
                                param1: $scope.model.deviceinfo.DUID,
                                param2: 'Click',
                                param3: key,
                                param4: 'false'
                            }
                        })))
                    return ('Success, message sent');
                }
                else {
                    return ('error, message not sent');
                }
            }
        }
        catch(e) {
            console.log('error', e);
            $location.path('/');
        }}


    ]); //close controller
});
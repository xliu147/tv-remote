'use strict';
define([
    'angular',
    'angularRoute',
    '../services/index'
], function(angular,services) {


    var app = angular.module('tvremoeteApp.view1', ['ngRoute','sessionService']);
        app.config(['$routeProvider', function($routeProvider) {

            $routeProvider.when('/', {
                templateUrl: 'views/ipinput.html',
                controller: 'mainController'
            });
        }]);

    var networkDiscover = function(callback) {
        var availableDevices = [];
        var serviceType = "ssdp:all";

        var success = function(devices) {
            for (var device in devices) {
                if ( devices[device]["SERVER"].includes("Samsung UPnP SDK") ) {
                    var ip = extractIP( devices[device]["LOCATION"] );

                    if ( availableDevices.indexOf(ip) < 0 ) {
                        availableDevices.push(ip);
                    }
                    //console.log('IP Address' + extractIP( devices[device]["LOCATION"] ) + ' is active');
                }
            }
            callback(availableDevices);
        }

        var failure = function() {
            alert("Error calling Service Discovery Plugin");
        }

        /**
         * Similar to the W3C specification for Network Service Discovery api 'http://www.w3.org/TR/discovery-api/'
         * @method getNetworkServices
         * @param {String} serviceType e.g. "urn:schemas-upnp-org:service:ContentDirectory:1", "ssdp:all", "urn:schemas-upnp-org:service:AVTransport:1"
         * @param {Function} success callback an array of services
         * @param {Function} failure callback
         */
        return serviceDiscovery.getNetworkServices(serviceType, success, failure);

    }



    app.controller('mainController', ['$scope','$location','$http','tvService', function($scope,$location,$http,tvService) {
            $scope.message = "enter IP address";




        $scope.searching = false;
        //screen 1
        $scope.discover = function() {
            $scope.deviceList = [];
            $scope.searching = true;
             var disco = networkDiscover(function(devices) {

                 var device = [{
                     info : null
                 }]

                for (var i in devices) {
                    var url = 'http://' + devices[i] + ':8001/ms/1.0/';
                    $http.get(url)
                        .then(function (response) {
                            device[i].info = response.data;
                            $scope.deviceList.push(device[i]);
                            console.log('bosh');
                            console.log($scope.deviceList);
                        });
                }
                $scope.searching = false;

            });

            console.log('full', $scope.deviceList);

        }


        $scope.connect = function(info) {
            $scope.model = tvService.getData();
            $scope.model.ipAddress = info.IP;
            $scope.model.pin = '';
            $scope.model.deviceinfo = info;
            $location.path('/pin');
        }

            }]);




})
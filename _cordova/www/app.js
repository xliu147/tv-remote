
'use strict';

define([
    'angular',
    'angularRoute',
    'views/view1',
    'views/view2'
], function(angular, angularRoute, view1, view2) {
    // Declare app level module which depends on views, and components





    return angular.module('tvremoeteApp', [
        'ngRoute',
        'tvremoeteApp.view1',
        'tvremoeteApp.view2'
    ]).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/view1'});
    }]);

});

//DATA MODEL
var tvData = function (obj) {
    var self = this;

    if (!obj) {
        self.ipAddress = null;
        self.pin = null;
        self.friendlyname = null;
        self.appID = '721b6fce-4ee6-48ba-8045-955a539edadb';
        self.userID = '654321';
        self.deviceinfo = null;
        self.paired = null;
        self.connectionstate = null;

    } else {
        self = obj;
    }

    return self;
}

//Construct pairing URI's from config and step
var buildPairingStepUri = function(step, data) {
    var path = '/ws/pairing?step=' + step + '&app_id=' + data.appID + '&device_id=' + data.deviceinfo.DeviceID;
    return 'http://' + data.ipAddress + ':8080' + path;
}

//Used for building the key events
var buildEmitMessage = function(name, payload) {
    return '5::/com.samsung.companion:' + JSON.stringify({
        name: name,
        args: [
            payload
        ],
    })
}

//Extract IP from string
var extractIP = function(str) {
    try {
        //RegEx
        var r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        var t = str.match(r);
        return t[0];
    }
    catch(e) {
        return e;
    }
}


